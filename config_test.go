package main

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func Test_readConfig(t *testing.T) {
	tests := []struct {
		name string
		env  map[string]string
		want *Config
		err  string
	}{
		{
			name: "invalid port",
			env: map[string]string{
				"HTTP_PORT": "foo",
			},
			err: `strconv.Atoi: parsing "foo": invalid syntax`,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for k, v := range tt.env {
				t.Setenv(k, v)
			}
			got, err := readConfig()
			switch {
			case tt.err == "" && err == nil:
				// All good
			case tt.err == "" && err != nil:
				t.Errorf("Got an error, expected none: %s", err)
				return
			case tt.err != "" && err == nil:
				t.Error("Expected an error, got none")
			case tt.err != err.Error():
				t.Errorf("Got an unexpected error: %s", err)
				return
			case err != nil:
				return
			}
			if d := cmp.Diff(tt.want, got); d != "" {
				t.Errorf("Unexpected result: %v", d)
			}
		})
	}
}
