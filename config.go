package main

import (
	env "github.com/Netflix/go-env"
)

// Config represents the application configuration which is
// read from the environment.
type Config struct {
	DatabaseDSN string `env:"DB_DSN,required=true"`
	HTTPPort    int    `env:"HTTP_PORT,required=true"`
}

func readConfig() (*Config, error) {
	cf := new(Config)
	_, err := env.UnmarshalFromEnviron(cf)
	return cf, err
}
