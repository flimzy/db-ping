module gitlab.com/flimzy/db-ping

go 1.17

require (
	github.com/Netflix/go-env v0.0.0-20210215222557-e437a7e7f9fb
	github.com/google/go-cmp v0.5.7
	github.com/lib/pq v1.10.4
)
